/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Data.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 *
 * @author javier
 */
public class DataInsert {

    private Connection con;

    public DataInsert() {
        Connect data = new Connect();
        this.con = data.StartConection();
    }

    public boolean check() {
        return this.con != null;
    }

    public String addSeet(String username, String key, String value) {
        try {
            PreparedStatement stmt = this.con.prepareStatement("insert into users_data (users_user_id, users_data_key, users_data_value) values ((select user_id from examen_jsp.users where user_name = ?), ?, ?);");
            stmt.setString(1, username);
            stmt.setString(2, key);
            stmt.setString(3, value);
            stmt.executeUpdate();
            stmt.close();
            return "1";
        } catch (Exception e) {
        return e.getMessage();
        }
    }

    public String addUser(String username, String password) {
        try {
            PreparedStatement stmt = this.con.prepareStatement("INSERT INTO users (user_name, user_password) VALUES (?, ?);");
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.executeUpdate();
            stmt.close();
            return "1";
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
