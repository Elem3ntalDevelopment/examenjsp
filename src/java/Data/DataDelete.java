/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author javier
 */
public class DataDelete {
    
    private Connection con;

    public DataDelete() {
        Connect data = new Connect();
        this.con = data.StartConection();
    }

    public boolean check() {
        return this.con != null;
    }

    public boolean deleteSet(String username) {
        try {
            PreparedStatement stmt = this.con.prepareStatement("delete from users_data where users_user_id = (select user_id from users where user_name = ?);");
            stmt.setString(1, username);
            stmt.executeUpdate();
            stmt.close();
            return true;
        } catch (Exception e) {
        }
        return false;
    }
}
