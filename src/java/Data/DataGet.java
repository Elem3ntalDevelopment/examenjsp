/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author javier
 */
import Data.Connect;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DataGet {

    private Connection con;

    public DataGet() {
        Connect data = new Connect();
        this.con = data.StartConection();
    }

    public boolean check() {
        return this.con != null;
    }

    public String loginUser(String username, String password) {
        try {
            Statement stmt = this.con.createStatement();
            String query = String.format("SELECT IF(STRCMP('%s',user_password),'not-valid','valid') AS response FROM users WHERE user_name = '%s'", password, username);
            ResultSet rst = stmt.executeQuery(query);
            while (rst.next()) {
                return rst.getString(1);
            }
            return "not-valid";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public List<String[]> getConfigUser(String username) {
        List<String[]> values = new ArrayList<String[]>();
        try {
            String query = String.format("select users_data_key as k, users_data_value as val from examen_jsp.users_data, examen_jsp.users\n"
                    + "where users_data.users_user_id = users.user_id\n"
                    + "and users.user_name = '%s';", username);
            
            Statement stmt = this.con.createStatement();
            ResultSet rst = stmt.executeQuery(query);
            while (rst.next()) {
                values.add(new String[]{rst.getString(1), rst.getString(2)});
            }
            return values;
        } catch (Exception e) {
            return null;
        }
    }

    public List<String> tables() {
        List<String> values = new ArrayList<String>();
        try {
            Statement stmt = this.con.createStatement();
            ResultSet rst = stmt.executeQuery("SHOW TABLES");
            while (rst.next()) {
                values.add(rst.getString(1));
            }
        } catch (Exception e) {
            values.add("-1");
            values.add(e.getMessage());
        }
        return values;
    }

}
