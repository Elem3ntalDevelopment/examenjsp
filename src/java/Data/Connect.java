/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author javier
 */
import java.sql.DriverManager;
import java.sql.Connection;

public class Connect {
    public String database = "examen_jsp";
    public String direction = "127.0.0.1";
    public String port = "3306";
    public String user = "root";
    public String password = "contrasenia";
    public Connect() {
    }
    
    public Connection StartConection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String connectionString = String.format("jdbc:mysql://%s:%s/%s", 
                    this.direction, this.port, this.database);
            con = DriverManager.getConnection(connectionString, 
                                              this.user, this.password);

        } catch (Exception e) {
            System.out.println("Error MySQL: " + e);
        }
        return con;
    }
}
