<%-- 
    Document   : internal
    Created on : 09-09-2018, 21:08:29
    Author     : javier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List" %>
<%@page import="Data.DataGet" %>
<%
    String validLogin = "0";
    int elements = 0;
    List<String[]> dataUser = null;
    DataGet database = new DataGet();
    if (request.getMethod().toString().compareTo("POST") == 0) {
        String username = request.getParameter("username");
        String password1 = request.getParameter("password");
        if (password1.length() > 0 && username.length() > 0) {
            validLogin = database.loginUser(username, password1);
            if (validLogin.compareTo("valid") == 0) {
                dataUser = database.getConfigUser(username);
                session.setAttribute("sessname", username);
            }
        }
    }
    if (session.getAttribute("sessname") != null) {
        String username = session.getAttribute("sessname").toString();
        dataUser = database.getConfigUser(username);
        elements = dataUser.size();
    }
    else{
        response.sendRedirect("index.jsp");
        return;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="RESOURCES/head.html" %> 
    </head>
    <body>
        <p class="title">response data</p>
        <a class="float-right btn" href="logout.jsp">Logout!</a>
        <%if (dataUser != null) {%>
        <div class='card container' >
            <form action="saveData.jsp" method="post" >
                <div id="data-user">
                    <% for (String[] data : dataUser) {%>
                    <div class="row">
                        <div col="col-xs-3 col-sm-3">
                            <input type="text" name='key-<%= dataUser.indexOf(data)%>' value="<%=data[0]%>">
                        </div>
                        <div col="col-xs-3 col-sm-3">
                            <input type="text" name='val-<%= dataUser.indexOf(data)%>' value="<%=data[1]%>">
                        </div>
                    </div>
                    <% } %>
                </div>
                <input class="col-xs-6 col-sm-2 btn" type="submit" value="Guardar">
            </form>
            <input class="col-xs-6 col-sm-2 btn" type="button" value="Añadir Campos" onclick='addFields()'>

        </div>
        <% } %>
        <%if (validLogin.compareTo("not-valid") == 0) {%>
        <div class="alert alert-danger">
            <p>Usuario/Contraseña incorrectos!</p>
            <a href="index.jsp">Volver Atras!</a>
        </div>
        <% }%>
    </body>
    <script  type="application/javascript">
        var counter=parseInt(<%= elements %>);
        console.log(counter);
        function addFields(){
            layout = $("#layout-row").html();
            layout = layout.replace("#",counter);
            layout = layout.replace("#",counter);
            $("#data-user").append(layout);
            counter++;
        }
    </script>
    <script id="layout-row" type="text/html">
        <div class="row">
            <div col="col-xs-6 col-sm-3">
                <input type="text" name='key-#' >
            </div>
            <div col="col-xs-6 col-sm-3">
                <input type="text" name='val-#' >
            </div>
        </div>
    </script>
</html>
