-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema examen_jsp
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `examen_jsp` ;

-- -----------------------------------------------------
-- Schema examen_jsp
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `examen_jsp` DEFAULT CHARACTER SET utf8 ;
USE `examen_jsp` ;

-- -----------------------------------------------------
-- Table `examen_jsp`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `examen_jsp`.`users` ;

CREATE TABLE IF NOT EXISTS `examen_jsp`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(45) NOT NULL,
  `user_password` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `examen_jsp`.`users_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `examen_jsp`.`users_data` ;

CREATE TABLE IF NOT EXISTS `examen_jsp`.`users_data` (
  `users_data_id` INT NOT NULL AUTO_INCREMENT,
  `users_user_id` INT NOT NULL,
  `users_data_key` VARCHAR(45) NOT NULL,
  `users_data_value` TEXT NOT NULL,
  PRIMARY KEY (`users_data_id`, `users_user_id`),
  INDEX `fk_users_data_users_idx` (`users_user_id` ASC),
  UNIQUE INDEX `together` (`users_data_key` ASC, `users_user_id` ASC),
  CONSTRAINT `fk_users_data_users`
    FOREIGN KEY (`users_user_id`)
    REFERENCES `examen_jsp`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
