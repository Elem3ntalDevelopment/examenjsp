<%-- 
    Document   : register
    Created on : 09-09-2018, 22:04:41
    Author     : javier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Data.DataInsert" %>
<%
    String registerValid = "not-valid";
    if (request.getMethod().toString().compareTo("POST") == 0) {
        DataInsert data = new DataInsert();
        String username = request.getParameter("username");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        if(password1.equals(password2)){
            registerValid = data.addUser(username, password1);
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="RESOURCES/head.html" %> 
    </head>
    <body>
        <p class="title">registro</p>
        <div class="container form-auth col-xs-12 col-sm-5 col-md-4 col-lg-3" id="loggin">
            <form action="register.jsp" method="post" autocomplete="off">
                <div class="row">
                    <input class="col-10 offset-1" name="username" type="text" placeholder="Username" required>                    
                </div>
                <div class="row">
                    <input class="col-10 offset-1" name="password1" type="password" placeholder="password" required>
                </div>
                <div class="row">
                    <input class="col-10 offset-1" name="password2" type="password" placeholder="retype password" required>
                </div>
                <div class="row">
                    <input class="col-10 offset-1 btn" type="submit" value="Registrar">
                </div>
                <div class="row">
                    <% if (registerValid=="not-valid") { %>
                        <a class="col-10 offset-1" href="index.jsp">estas registrado? click aquí!</a>
                    <% } else {%>
                        <a class="col-10 offset-1" href="index.jsp">Usuario ha sido registrado! click aquí!</a>

                    <% }%>

                </div>
            </form>
        </div>
    </body>
</html>
