<%-- 
    Document   : Index
    Created on : 09-09-2018, 19:02:31
    Author     : javier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <%@ include file="RESOURCES/head.html" %> 
    </head>
    <body>
        <p class="title">acceso</p>
        <div class="container form-auth col-xs-12 col-sm-5 col-md-4 col-lg-3" id="loggin">
            <form action="response.jsp" method="post" autocomplete="off">
                <div class="row">
                    <input class="col-10 offset-1" name="username" type="text" placeholder="Username" required>                    
                </div>
                <div class="row">
                    <input class="col-10 offset-1" name="password" type="password" placeholder="password" required>
                </div>
                <div class="row">
                    <input class="col-10 offset-1 btn" type="submit" value="Ingresar">
                </div>
                <div class="row">
                    <a class="col-10 offset-1" href="register.jsp">No estas registrado? click aquí</a>
                </div>
            </form>
        </div>
    </body>
</html>
